const exec = require('child_process').exec;
const workingDirectory = require(`${__dirname}/../config.json`).WORKING_DIRECTORY;
const allowedCommands = require(`${__dirname}/../config.json`).ALLOWED_COMMANDS;

/**
 * Check entered params and return only valid params
 * @param commands - array of args from slack
 * @returns {Array}
 */
function getParams(commands) {
  if (commands.length) {
    const validCommands = {};
    const argsList = commands.map(command => {
      const splitedCommand = command.split('=');
      if (splitedCommand.length == 2) {
        const key = splitedCommand[0];
        const value = splitedCommand[1];
        const isValid = argValidation(key, value);

        if (isValid) {
          validCommands[key] = value;
          return key;
        }
        return false;
      }
      return false;
    }).filter(item => {
      return item !== false;
    });

    return argsList.map(item => {
      return `${item}=${validCommands[item]}`;
    });
  } else {
    return [];
  }
}

/**
 * Validate allowed params
 * @param key
 * @param value
 * @returns {boolean}
 */
function argValidation(key, value) {
  switch(key) {
    case '--month':
      return dateValidation(value);
      break;
    default:
      return false;
  }
}

/**
 * Date validation
 * @param yyyymm - 201612
 * @returns {boolean}
 */
function dateValidation(yyyymm) {
  const month = yyyymm % 100;
  return ((month > 0) && (month < 13));
}

module.exports = robot => {
  let previousMsg = {
    scriptName: '',
    scriptExecDate: ''
  };
  robot.respond(/hubot (.*)/i, function(res) {
    const command = res.match[1].match(/\S+/g) || [];
    const scriptName = command.shift();

    const params = getParams(command);
    const firstParameter = params.length ? params.shift() : false;

    if (Object.keys(allowedCommands).includes(scriptName)) {
      const expression = firstParameter ? `${workingDirectory}${scriptName} ${firstParameter}` : `${workingDirectory}${scriptName}`;
      exec(`python ${expression}`, (error, stdout, stderr) => {
        if (error) {
          console.error('exec error: ' + error);
          res.reply('Script execution error!!!');
          return;
        }
        if (stderr) {
          console.log(stderr);
          res.reply('STDERR Error in script processing');
          return;
        }
        res.reply(`Script <${scriptName}> execution is finished`);
      });
    } else {
      res.reply('Invalid command!!!');
    }
  })

};
